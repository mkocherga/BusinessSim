﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class testScript : MonoBehaviour {

    private int _money;
    public Text moneyText;
    public int addingCount;

    void Start()
    {
        SetRandomMoneyValue();
    }

    void SetRandomMoneyValue()
    {
        money = Random.Range(0, 100000);
    }

    public string InsertSpaces(string number)
    {
        for (int i = 0; i < number.Length; i += 4)
        {
            number = number.Insert(number.Length - i, " ");
        }
        return number;
    }

    public int money
    {
        get
        {
            return _money;
        }
        set
        {
            _money = value;
            if (Mathf.FloorToInt(_money / 1000) > 1)
            {
                moneyText.text = InsertSpaces(_money.ToString());
            }
            else
            {
                moneyText.text = _money.ToString();
            }
        }
    }

    public void AddMoney()
    {
        StartCoroutine(AddingCor());
    }

    IEnumerator AddingCor()
    {
        int targerMoney = money + addingCount;
        while (money < targerMoney)
        {
            money += (int)Mathf.Floor(addingCount / 60f);
            yield return new WaitForSeconds(Time.deltaTime);
        }
        if (money > targerMoney)
        {
            money = targerMoney;
        }
    }
}
