﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ExtraAttentionUI : MonoBehaviour {

    public static ExtraAttentionUI instance;
    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }

    List<IPost> posts = new List<IPost>();
    public GameObject postIconPrefab,acceptButton,declineButton,nextButton;
    public Text header,description;
    public Image postIcon,mainPostImage;
    IPost currentPost;
    int curPostId;
    public void addPost(IPost post)
    {
        curPostId = posts.Count;
        currentPost = post;
        updateView();
        if(post is ExtraPost)
        {
            posts.Add(post);
        }
    }
    void updateView()
    {
        ItemDatabase db = ItemDatabase.instance;
        header.text = currentPost.header;
        description.text = currentPost.message;
        if (currentPost is MainPost)
        {
            MainPost mainpost = (MainPost)currentPost;
            string path = "Images/" + mainpost.itemid;
            string itemid = mainpost.itemid;
            if (mainpost.itemid.Contains("_"))
            {
                itemid = itemid.Split('_')[1];
            }
            mainPostImage.sprite = Resources.Load<Sprite>(path);
            if (db.sprites.ContainsKey(itemid))
            {
                postIcon.sprite = db.sprites[itemid];
            }
        }
        else
        {
            if (currentPost is ExtraPost)
            {
                ExtraPost extraPost = (ExtraPost)currentPost;
                string path = "Images/" + extraPost.item.id;
                mainPostImage.sprite = Resources.Load<Sprite>(path);
                if (db.sprites.ContainsKey(extraPost.item.id))
                {
                    postIcon.sprite = db.sprites[extraPost.item.id];
                }
            }
        }
        description.text += " ";
    }
    public void openNextPost()
    {
        if (curPostId > 0)
        {
            currentPost = posts[--curPostId];
            updateView();
        }
        else
        {
            gameObject.SetActive(false);
        }
        
    }
}
