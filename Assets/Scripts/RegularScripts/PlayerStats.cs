﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
[Serializable]
public class PlayerStats{

    //raw stats
    public static PlayerStats instance;

	public float capital{get;set;}
	public float baseCustomers, customers, baseCheck, goal, costPrice, basePower;
	public float businessPower, capital_per_day, netCost;

//	public bool socialMedia = false, webSite = false;

    public float dailyCapital = 0, dailyBPower, dailyCustomers, dailyProfit,lastDailyProfit,dailyCheck,dailyNetCost;
//    public float weeklyCapital, weeklyBPower, weeklyCustomers, weeklyProfit,weeklyCheck, weeklyNetCost;
//    public float monthlyCapital, monthlyBPower, monthlyCustomers, monthlyProfit, monthlyCheck, monthlyNetCost;

	//calculating stats
	public float costs, income, check, baseNetCost;

	private PlayerStats(string bType)
    {
        resetStats(bType);
    }
    public static PlayerStats getInstance()
    {
        if(instance == null)
        { 
		//	Debug.Log ("btype"+GameManager.instance.bType);
			if (GameManager.instance.bType != "") {
				instance = new PlayerStats (GameManager.instance.bType);
			} else { //костыль чтобы избужать nullreference TODO переделать позже
				instance = new PlayerStats ("Coffe");
			}
        }
        return instance;
    }


	public void resetStats(String bType){
		if (bType.Equals ("Coffe")) {
			basePower = 20;
			baseCustomers = 10;
			baseCheck = 120;
			baseNetCost = 50;
		} else if (bType.Equals ("Salon")) {
			basePower = 3;
			baseCustomers = 4;
			baseCheck = 1500;
			baseNetCost = 500;
		} else if (bType.Equals ("Furniture")) {
			basePower = 4;
			baseCustomers = 4;
			baseCheck = 2000;
			baseNetCost = 1300;
		} else {
			Debug.Log(" bType is incorrect");
			basePower = 0;
			baseCustomers = 0;
			baseCheck = 0;
			baseNetCost = 0;
		}
        capital = 0;
		capital_per_day = 0;
		goal = 0;

		customers= baseCustomers;
		check = baseCheck;
		netCost = baseNetCost;
		businessPower = basePower;

		costs = 50;
		income = 0;
		dailyProfit =0;

		costPrice = 30;

		dailyCheck = baseCheck;
		dailyBPower = basePower;
		dailyNetCost = netCost;
		dailyCustomers = 0;
		dailyCustomers = baseCustomers;
		dailyCapital = 0;

//		socialMedia = false;
//		webSite = false;
//		monthlyCapital = 0;


//		weeklyCheck = baseCheck;
//		monthlyCheck = baseCheck;


//		weeklyNetCost = netCost;
//		monthlyNetCost = netCost;
        
//        weeklyCustomers = baseCustomers;
//        monthlyCustomers = baseCustomers;

	}
}
