﻿
using UnityEngine;

public class DangerMainPost : MainPost {

    public DangerMainPost(string header, string message, string itemid, bool success) : base(header, message, itemid, success)
    {
    }
    public override void createPost()
    {
        
        PostsManager postManager = PostsManager.instance;

        GameObject newPost = postManager.createPrefab(postManager.mainPostPrefab);
        newPost.GetComponent<MainPostUI>().fill(this);
        newPost.transform.SetParent(postManager.mainContent, false);
        newPost.transform.SetSiblingIndex(1);
        
    }
}
