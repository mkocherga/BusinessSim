﻿using System;
using UnityEngine;

public class PreviewPost : IPost
{
    public string header;
    public IPost post;
    public string itemid;
    public PreviewPost(IPost post,string header, string itemid)
    {
        this.post = post;
        this.header = header;
        this.itemid = itemid;
    }
    public override void createPost()
    {
        PostsManager postManager = PostsManager.instance;
        GameObject newPost = postManager.createPrefab(postManager.previewPostPrefab);
        newPost.GetComponent<PreviewPostUI>().fill(itemid, itemid, header);
        newPost.transform.SetParent(postManager.mainContent, false);
        newPost.transform.SetSiblingIndex(1);
    }
}
