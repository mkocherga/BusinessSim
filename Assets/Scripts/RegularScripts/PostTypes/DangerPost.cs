﻿using UnityEngine;

public class DangerPost : ExtraPost
{
    public DangerPost(string header, string message, string slotname, Item item) : base(header, message, slotname, item)
    {
    }
    public override void createPost()
    {
        PostsManager postManager = PostsManager.instance;

        GameObject newPost = postManager.createPrefab(postManager.extraPostPrefab);
        GameObject extraNewPost = postManager.createPrefab(postManager.extraPostPrefab);

        newPost.transform.SetParent(postManager.mainContent, false);
        newPost.transform.SetSiblingIndex(1);
        //newPost.transform.SetAsFirstSibling();

        extraNewPost.transform.SetParent(postManager.extraContent, false);
        extraNewPost.transform.SetSiblingIndex(1);
        //extraNewPost.transform.SetAsFirstSibling();

        newPost.GetComponent<PostUI>().fill(this, extraNewPost);
        extraNewPost.GetComponent<PostUI>().fill(this, newPost);

        extraNewPost.GetComponent<PostUI>().setTextToDanger();
        newPost.GetComponent<PostUI>().setTextToDanger();

    }
}
