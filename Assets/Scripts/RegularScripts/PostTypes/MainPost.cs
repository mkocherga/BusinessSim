﻿using System;
using UnityEngine;

public class MainPost : IPost
{
    public string itemid;
    public bool success = false;

    public MainPost(string header, string message, string itemid,bool success)
    {
        this.header = header;
        this.message = message;
        this.itemid = itemid;
        this.success = success;
    }

    public override void createPost()
    {
        Item item = ItemDatabase.instance.getItem(itemid);
        PostsManager postManager = PostsManager.instance;
        
        GameObject newPost = postManager.createPrefab(postManager.mainPostPrefab);
        newPost.GetComponent<MainPostUI>().fill(this);
        newPost.transform.SetParent(postManager.mainContent, false);
        newPost.transform.SetSiblingIndex(1);
        
        
        if (item.workingQuality != null)
        {
            postManager.createEntries(newPost.transform.GetChild(2).gameObject, item, success);
        }
    }
}
