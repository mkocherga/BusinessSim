﻿using System;
using UnityEngine;

public class ExtraPost : IPost
{
    public Item item;
    public string slotname;

    public ExtraPost(string header, string message, string slotname, Item item)
    {
        this.header = header;
        this.message = message;
        this.item = item;
        this.slotname = slotname;
    }

    public override void createPost()
    {
        PostsManager postManager = PostsManager.instance;

        GameObject newPost = postManager.createPrefab(postManager.extraPostPrefab);
        GameObject extraNewPost = postManager.createPrefab(postManager.extraPostPrefab);

        newPost.transform.SetParent(postManager.mainContent, false);
        newPost.transform.SetSiblingIndex(1);

        extraNewPost.transform.SetParent(postManager.extraContent, false);
        newPost.transform.SetSiblingIndex(1);

        newPost.GetComponent<PostUI>().fill(this, extraNewPost);
        extraNewPost.GetComponent<PostUI>().fill(this, newPost);
    }
}
