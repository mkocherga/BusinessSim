﻿using System.Collections.Generic;
using UnityEngine;

public class ReportPost : IPost
{
    public string mainHeader;
    public List<ParamValue> paramValues;

    public ReportPost(string mainHeader, List<ParamValue> paramValues)
    {
        this.mainHeader = mainHeader;
        this.paramValues = paramValues;
    }

    public void addParam(string header,string totalValue, string periodValue,string id )
    {
        paramValues.Add(new ParamValue(header,totalValue,periodValue,id));
    }

    public override void createPost()
    {
        PostsManager postManager = PostsManager.instance;

        GameObject newPost = postManager.createPrefab(postManager.reportPostPrefab);
        newPost.transform.SetParent(postManager.mainContent, false);
        newPost.transform.SetSiblingIndex(1);
        //newPost.transform.SetAsFirstSibling();
        newPost.GetComponent<ReportPostUI>().setPost(this);
    }

    public class ParamValue
    {
        public string header;
        public string totalValue;
        public string periodValue;
        public string id;

        public ParamValue(string header, string totalValue, string periodValue,string id)
        {
            this.id = id;
            this.header = header;
            this.totalValue = totalValue;
            this.periodValue = periodValue;
        }
    }
}
