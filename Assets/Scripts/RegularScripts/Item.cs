﻿using System.Collections.Generic;
using System;
[Serializable]
public class Item
{

    public string id;
    public string type;
    public int maxLevel = -1;
    public string name;
    public string description;
    public BreakDanger activeDanger;
    public List<Quality> possibleQualities { get; set; }

    public Quality actualQuality;
    public Quality workingQuality;
    public string successHeader;
    public float chanceDangerBefore;
    [Newtonsoft.Json.JsonConstructor]
    public Item(string id, string type, string name, string description, List<Quality> possibleQualities, Quality actualQuality, string successHeader, float chanceDangerBefore)
    {
        this.id = id;
        this.type = type;
        this.name = name;
        this.description = description;
        this.possibleQualities = possibleQualities;
        this.actualQuality = actualQuality;
        this.successHeader = successHeader;
        this.chanceDangerBefore = chanceDangerBefore;
    }
	//Возращает количество уровней качества для текущего инструмента
    public int getMaxLevel()
    {
        if(maxLevel == -1)
        {
            maxLevel = possibleQualities[possibleQualities.Count - 1].qualityId;
        }
        return maxLevel;
    }
    public bool isMaxLevel()
    {
        getMaxLevel();
        if (actualQuality!=null && actualQuality.qualityId == maxLevel)
        {
            return true;
        }
        return false;
    }
	//Принимает на вход условие, возвращает true если оно есть среди условий у actualQuality 
    public bool dependent(String id)
    {
        if (actualQuality != null && actualQuality.conditions != null)
        {
            if (actualQuality.conditions.Contains(id))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
    }


    public bool isEnoughMoney()
    {
        float capital = GameManager.instance.getStats().capital;

        if (activeDanger != null) //если интсрумент сломан, проверяет достаточно ли денег для починки
        {
            return capital > actualQuality.capital * 0.1 ? true : false;
        }
        else //проверяем достаточно ли денег для покупки инструмента
        {
            return capital > workingQuality.capital ? true : false;
        }
    }

	//Применение эффекта от инструмента
    public void activate()
    {
        PlayerStats stats = GameManager.instance.getStats();
        actualQuality = workingQuality;

        stats.businessPower += (actualQuality.businesPower ) * stats.basePower;
		stats.customers += (actualQuality.customers) * stats.baseCustomers;
        stats.capital_per_day += actualQuality.capitalPerDay;
        stats.check += (actualQuality.check) * stats.baseCheck;
        stats.netCost += (actualQuality.netCost ) * stats.baseNetCost;
    }

	//Убирает инструмент и его воздействие на бизнес
	public void deactivate()
	{
		PlayerStats stats = GameManager.instance.getStats();
		stats.businessPower -= (actualQuality.businesPower ) * stats.basePower;
		stats.customers -= (actualQuality.customers) * stats.baseCustomers;
		stats.capital_per_day -= actualQuality.capitalPerDay;
		stats.check -= (actualQuality.check ) * stats.baseCheck;
		stats.netCost -= (actualQuality.netCost) * stats.baseNetCost;
		actualQuality = null;
	}

	//Наносит необходимый ущерб при поломке инструмента
	public void Break(BreakDanger danger){
		if(actualQuality==null){
			throw new Exception("Something wrong!");
		}
		activeDanger = danger;
		PlayerStats stats = GameManager.instance.getStats();
		stats.capital -= danger.capital;
		stats.businessPower -= stats.basePower *actualQuality.businesPower*danger.bPower;
		stats.customers -= stats.baseCustomers * actualQuality.customers * danger.customers;
		stats.capital_per_day -= actualQuality.capitalPerDay;
		stats.netCost += stats.baseNetCost*actualQuality.netCost*danger.netCost;
	}

	//Убирает негативный эфект от "поломки" инстурмента
    public void Repair(){
        PlayerStats stats = GameManager.instance.getStats();
        stats.businessPower += stats.basePower*actualQuality.businesPower*activeDanger.bPower;
        stats.customers += stats.baseCustomers*actualQuality.customers*activeDanger.customers;
        stats.capital_per_day += actualQuality.capitalPerDay;
        stats.netCost -= stats.baseNetCost*actualQuality.netCost*activeDanger.netCost;
        activeDanger = null;
    }

	public bool available(Dictionary<string, Item> learnedSkills)
	{
		foreach (Quality quality in possibleQualities)
		{
			if (quality.available(learnedSkills))
			{
				return true;
			}
		}
		return false;
	}

		
}
