﻿using System.Collections.Generic;
using System;

[Serializable]
public class Quality {

	//i feel like there is much better way to do this, but its fine for now.
	//название уровня кач-ва
	public string qualityLevel;

    public int qualityId;
	//chances specific to particular quality
	public float chanceDangerLost;
	public float chanceDangerBreak;
	public float chanceOfSuccess;
	public float chanceOfFailure;
	public float chanceOfMoreTime;
    public List<string> conditions { get; set; }
    //message for particular cases
    public string messageSuccess {get;set;}
	public string messageFailure {get; set;}
	public string messageMoreTime {get;set;}

	//main stats of item
	public float businesPower,customers,time,capital,capitalPerDay,check,netCost,ifFailCapital,ifMoreTime;
	

    public bool available(Dictionary<string,Item> learnedSkills)
    {
        if (conditions == null)
        {
            return true;
        }
        if(conditions.Count>0&& learnedSkills == null)
        {
            return false;
        }
        int totalMatches = 0;
        for(int i = 0; i < conditions.Count; i++)
        {
            if (learnedSkills.ContainsKey(conditions[i]))
            {
                totalMatches++;
            }
        }
        if (totalMatches == conditions.Count)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

	[Newtonsoft.Json.JsonConstructor]
	public Quality(string qualityLevel,int qualityId,float chanceOfSuccess,float chanceOfFailure, float chanceOfMoreTime, 
		float chanceDangerLost, float chanceDangerBreak,
		string messageSuccess,string messageFailure, string messageMoreTime,
		float businesPower,float customers,float time,float capital,float ifFailCapital,float ifMoreTime,float capitalPerDay,float check,float netCost, List<string> conditions)
    {

		this.qualityLevel = qualityLevel;
        this.qualityId = qualityId;
		this.ifMoreTime = ifMoreTime;
		this.ifFailCapital = ifFailCapital;
		this.chanceOfSuccess = chanceOfSuccess;
		this.chanceOfFailure = chanceOfFailure;
		this.chanceOfMoreTime = chanceOfMoreTime;
		this.chanceDangerLost = chanceDangerLost;
		this.chanceDangerBreak = chanceDangerBreak;
			
		this.messageSuccess = messageSuccess;
		this.messageMoreTime = messageMoreTime;
		this.messageFailure = messageFailure;

		this.businesPower = businesPower;
		this.customers = customers;
		this.time = time;
		this.capital = capital;
		this.capitalPerDay = capitalPerDay;
		this.check = check;
		this.netCost = netCost;
        this.conditions = conditions;
    }

}
