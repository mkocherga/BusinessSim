﻿
public class LostDanger
{
	public string id;
	public string message;
	public string header;

	public LostDanger (string id, string message, string header)
	{
		this.id = id;
		this.message = message;
		this.header = header;
	}
}

