﻿
public class BreakDanger {
    public string id;
    public string message;
    public string header;
    public float bPower, customers, capital, netCost;
    public bool capitalPerDay = false;

    public BreakDanger(string id, string message, string header, float bPower, float customers, float capital, bool capitalPerDay, float netCost)
    {
        this.id = id;
        this.message = message;
        this.header = header;
        this.bPower = bPower;
        this.customers = customers;
        this.capital = capital;
        this.capitalPerDay = capitalPerDay;
        this.netCost = netCost;
    }
}
