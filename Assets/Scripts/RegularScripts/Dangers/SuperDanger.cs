﻿using System.Collections.Generic;

public class SuperDanger
{
	public string id;
	public string message;
	public string header;
	public float chance;
	public float time;
	public float bPower, customers, capital, netCost;
	public List<string> conditions { get; set; }
	public SuperDanger (string id, string message, string header, float chance, float time, float bPower, float customers, float capital, bool capitalPerDay, float netCost, List<string> conditions)
	{
		this.id = id;
		this.message = message;
		this.header = header;
		this.chance = chance;
		this.time = time;
		this.bPower = bPower;
		this.customers = customers;
		this.capital = capital;
		this.netCost = netCost;
		this.conditions = conditions;

	}
}

