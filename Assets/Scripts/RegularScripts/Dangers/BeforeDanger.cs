﻿
public class BeforeDanger
{
	public string id;
	public string message;
	public string header;
	public float chance;
	public float bPower, customers, capital, netCost;
	
	public BeforeDanger (string id, string message, string header, float chance, float bPower, float customers, float capital, bool capitalPerDay, float netCost)
	{
		this.id = id;
		this.message = message;
		this.header = header;
		this.chance = chance;
		this.bPower = bPower;
		this.customers = customers;
		this.capital = capital;
		this.netCost = netCost;
	}
}


