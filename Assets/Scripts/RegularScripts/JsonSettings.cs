﻿using Newtonsoft.Json;

public class JsonSettings {

    public static JsonSerializerSettings settings = new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All };
}
