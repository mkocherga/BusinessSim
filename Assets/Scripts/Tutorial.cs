﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class Tutorial : MonoBehaviour {

    public static Tutorial instance;
    public GameObject tutorialUI;
    public Image background;
    public GameObject buttonsBackground,buttonAccpet,buttonDecline;
    public Button[] slots;
    public GameObject bottomTextPanel;
	public GameObject upperTextPanel;
    public GameObject bottomPanelButton;
    public GameObject upperText;
//	public GameObject upperText;
    public Text tutorialText;
    public GameObject[] hints;
    int currentStep = 0;
    public Image[] imagesToHighlight;
	public Image[] inputFieldsToHighLight;

    void Awake(){
        if (instance == null){
            instance = this;
        }
        else if (instance != this){
            Destroy(gameObject);
        }
    }

    void Start(){
        buttonAccpet.GetComponent<Button>().onClick.AddListener(OnAccept);
        buttonDecline.GetComponent<Button>().onClick.AddListener(onDecline);
    }

    void OnAccept(){
        //foreach(Button slot in slots)
        //{
        //    slot.interactable = false;
        //}
        buttonsBackground.SetActive(false);
        bottomTextPanel.SetActive(true);
        background.enabled = true;
		next ();
    }

    public void onDecline(){
        tutorialUI.SetActive(false);
    }

	//открытие панели доп информации
    public void onExpand(){
        if(currentStep == 4)
            next();
    }

	//закрытие панели доп информации
	public void onClose(){ 
		if (currentStep == 6)
			next();
	}

	//открытие панели инструментов
    public void onSkills(){
        if (currentStep == 8)
            next();
    }

	//Выбор инстурмента бизнес-план
    public void onChooser(){
        if (currentStep == 10)
            next();
    }

	//нажатие кнопки "Написать самостоятельно"
    public void onSelectQuality(){
        if (currentStep == 11)
            next();
    }

	public void onCreateBPlan(){
		if (currentStep == 14)
			next();
	}
    
    public void onNext()
    {
        next();
    }

	string text = "";
    public bool canBeClosed = true; 
    void next()
    {
		
		manageHint (currentStep-1); //рисуем стрелочки
		manageHighlightedImage (currentStep-1); //подсвечиваем нужную область
			
		switch (currentStep) {
		case 0:
			text = "Поздравляем! Вы открыли свой новый бизнес. Сейчас вы научитесь использовать основные возможности данного бизнес-симулятора.";
			break;
		case 1:
			text = "Справа сверху находится меню, где можно начать игру заново, получить ответы по правилам игры и увидеть таблиц результатов других игроков.";
			break;
		case 2:
			text = "Это ваш мини-календарь, он показывает сколько дней вы в бизнесе. Всего у вас 90 дней на то, чтобы добиться выдающихся результатов"; 
			break;
		case 3:
			text =  "Нажмите на иконку раскрытия меню"; 
			bottomPanelButton.SetActive(false);
			background.raycastTarget = false;
			break;
		case 4:
			text = "Здесь показана основная информация о том сколько вам доступно денег, ежедневный доход, мощность производства показывает сколько клиентов в день вы можете обслужить, теоретический охват аудитории показывает сколько потенциальных клиентов может совершить покупку у вас."; 
			bottomPanelButton.SetActive(true);
			canBeClosed = false;
			background.raycastTarget = true;
			break;
		case 5:
			text = "Себестоимость для одного чека и средний доход одного чека. Разница между этими параметрами показывает сколько чистой прибыли можно заработать с 1 клиента.  Здесь также показана Финансовая цель которую вы определили для себя на 12 недель и текущий показатель прогресса.  Нажмите \"свернуть\", чтобы продолжить"; 
			bottomPanelButton.SetActive(false);
			background.raycastTarget = false;
			canBeClosed = true; 
			break;
		case 6:
			text = "Планы на день. Здесь вы можете назначить задачи, которые помогут вашему бизнесу развиваться. Иногда в игре вам необходимо будет принимать решение о ремонте, это действие также занимает 1 слот. Грамотно распоряжайтесь доступными слотами."; 
			bottomPanelButton.SetActive(true);
			background.raycastTarget = true;


			break;
		case 7:
			text = "Для начала, давайте создадим Бизнес План, чтобы разобраться как выбирать и применять бизнес-инструменты. Нажмите на пустой слот."; 
			bottomPanelButton.SetActive(false);
			background.raycastTarget = false;
			break;
		case 8:
			text = "Создание Бизнес Плана находится во вкладке Бизнес."; 
			bottomPanelButton.SetActive(true);
			background.raycastTarget = true;
			break;
		case 9:
			text = "Выберите \"Бизнес План\""; 
			bottomPanelButton.SetActive(false);
			background.raycastTarget = false;
			break;
		case 10:
			upperText.SetActive (true);
			bottomTextPanel.SetActive (false);
			text = "Здесь показаны уровни качества бизнес-инструмента. Более высокий уровень качества стоит дороже, но шанс того, что исполнитель не выполнит заказ значительно ниже.Выберите подходящий."; 
			break;
		case 11:
			text = "Бизнес-план - это основа любого бизнеса. Именно на этом этапе оцениваются все основные затраты, ожидаемая прибыль, а также дается общая оценка бизнеса. Бизнес план также позволяет оценить результаты деятельности и окупаемость. Попробуй заполнить бизнес-план своего собственного дела!  "; 
			upperTextPanel.transform.GetChild (0).GetComponent<Text> ().text = text;
			background.raycastTarget = true;
			upperTextPanel.SetActive(true);
			//bottomPanelButton.SetActive(true);
			upperText.SetActive(false);
			break;
		case 12:
			text = "Перед тобой основные параметры любого бизнеса. Стоит серьезно подойти к оценки основных вложений и предполагаемой выручки. Попробуй рассчитать основные параметры для своего бизнес плана. "; 
			upperTextPanel.transform.GetChild (0).GetComponent<Text> ().text = text;
			hightlightInputField (true);
			break;
		case 13:
			
			text = ""; 
			background.enabled = false;
			upperTextPanel.SetActive(false);
			hightlightInputField (false);
			break;
		case 14:
			text = "Отлично, ты ввел все необходимые параметры, оказывающие серьезное влияние на твой бизнес. В конце игры ты сможешь оценить, полученный результат со своим бизнес-планом. Нажми кнопку завершить и приступай к созданию собственного бизнеса!"; 
			background.raycastTarget = true;
			background.enabled = true;
			bottomTextPanel.SetActive(true);
			bottomPanelButton.SetActive(true);
			bottomPanelButton.transform.GetChild(0).GetComponent<Text>().text = "Закончить";
			break;
//		case 15:
//			tutorialText.text = "Отлично, Первый шаг сделан! Обо всех изменениях в вашем бизнесе вы узнаете из ленты новостей."; 
//			break;
//		case 16:
//			tutorialText.text = "Будьте внимательны к своему бизнесу, следите за охватом аудитории, снижайте себестоимость и повышайте цены, покупайте оборудование и нанимайте персонал. Удачи!"; 
//			bottomPanelButton.transform.GetChild(0).GetComponent<Text>().text = "Закончить";
//			break;
		case 15: //Уничтожаем туториал на последнем шаге
			//Destroy (gameObject);
			gameObject.SetActive (false);
			GameManager.instance.CreateBPlan ();
			break;
		

		}
		tutorialText.text = text;
		currentStep++;
    }

	void hightlightInputField(bool param){
		foreach (Image inputField in inputFieldsToHighLight) {
			inputField.enabled = param;
		}
	}

	void manageHint(int n){
		if (n > 0 && n-1 < hints.Length)
			hints[n - 1].SetActive(false);
		if (n>=0 && n < hints.Length)
			hints[n].SetActive(true);
	}

	void manageHighlightedImage(int n){
		if (n > 0 && n-1 < imagesToHighlight.Length)
			imagesToHighlight[n - 1].enabled = false;
		if (n>=0 && n < imagesToHighlight.Length)
			imagesToHighlight[n].enabled = true;
	}

	public bool isActive(){
		return gameObject.activeInHierarchy;
	}
}
