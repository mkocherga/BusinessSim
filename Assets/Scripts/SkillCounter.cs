﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SkillCounter : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IScrollHandler,IPointerDownHandler
{
    ScrollRect mainScroll;
    bool activated = false;
    public Button btn;
    public GameObject checkImg;
    public GameObject parent;
    public Image itemIcon;
    public GameObject lockImg, timerImg;
    public GameObject unlocked;
    public GameObject[] stars;
    public Color nonActive;

    void Start()
    {
        //dont even ask
        mainScroll = gameObject.transform.parent.parent.parent.GetComponent<ScrollRect>();
    }

    public void showLevel(Item item)
    {
        int maxLevel = item.getMaxLevel() + 1;
        int level = item.actualQuality.qualityId + 1;
        if (activated)
        {
            hide();
        }
        activateHints();
        for (int i = 1; i < 4; i++)
        {
            if (i < level)
            {
                stars[i - 1].SetActive(true);
            }
            else
            {
                stars[i - 1].SetActive(false);
            }
        }
        setCompleted(level == maxLevel);
    }
    public void hideLevels()
    {
        foreach (GameObject img in stars)
        {
            img.SetActive(false);
        }
    }
    public bool somethingIsShown()
    {
        if (lockImg.activeSelf || timerImg.activeSelf || stars[0].activeSelf)
        {
            return true;
        }
        return false;
    }
    public void showLock()
    {
        if (activated)
        {
            hide();
        }
        else
        {
            activateHints();
        }
        lockImg.SetActive(true);
    }
    public void showTimer()
    {
        if (activated)
        {
            hide();
        }
        else
        {
            activateHints();
        }
        timerImg.SetActive(true);
    }
    public void hide()
    {
        lockImg.SetActive(false);
        timerImg.SetActive(false);
        unlocked.SetActive(false);
    }
    public void setCompleted(bool completed)
    {
        //checkImg.SetActive(completed);
    }

    public void setInteractable(bool interactable)
    {
        btn.interactable = interactable;
        if (interactable)
        {
            lockImg.SetActive(false);
            timerImg.SetActive(false);
            itemIcon.color = Color.white;
        }
        else
        {
            showLock();
            itemIcon.color = nonActive;
        }

        if (!somethingIsShown())
        {
            activated = false;
            parent.SetActive(false);
            unlocked.SetActive(true);
        }
        else
        {
            unlocked.SetActive(false);
        }
    }

    public bool isInteractable()
    {
        return btn.IsInteractable();
    }
    void activateHints()
    {
        if (!activated)
        {
            parent.SetActive(true);
            activated = true;
        }
    }
    void deactivateHints()
    {
        activated = false;
        parent.SetActive(false);
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        mainScroll.OnBeginDrag(eventData);
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (mainScroll.velocity.magnitude > 0)
        {
            canClick = false;
        }
        mainScroll.OnDrag(eventData);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        mainScroll.OnEndDrag(eventData);
    }
//
    public void OnScroll(PointerEventData eventData)
    {
        mainScroll.OnScroll(eventData);
    }
    public bool canClick = true;

    public void OnPointerDown(PointerEventData eventData)
    {
        
    }
}
