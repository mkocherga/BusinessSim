﻿using UnityEngine;
using UnityEngine.UI;

public class Chooser : MonoBehaviour {

    public Text description;
    public Text header;
    public Image mainImage;

    public void fill(string description,Sprite image,string header) 
    {
        mainImage.sprite = image;
        this.description.text = description;
        this.header.text = header;
    }
}
