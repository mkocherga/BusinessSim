﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class ItemSlotUI : MonoBehaviour,IPointerClickHandler {


    public Image myImage;
    public Text qualityName;
    public ConditionsUI conditionWindow;
    public Color defaultColor;
    public Color nonActive;

    Item item;
    Quality quality;
    GameManager manager;
    public Text capitalText,timeText;
    bool canBePerformed;

    public void setValues(Item item, Quality quality)
    {
        manager = GameManager.instance;
        this.item = item;
        this.quality = quality;
        qualityName.text = quality.qualityLevel;
        capitalText.text = quality.capital.ToString();
        timeText.text = quality.time.ToString();
        if (quality.available(manager.activeSkills))
        {
            if(quality.capital > manager.getStats().capital)
            {
                capitalText.color = Color.red;
            }
            else
            {
                capitalText.color = Color.white;

            }
            canBePerformed = true;
            GetComponent<Image>().color = defaultColor;
        }
        else
        {
            canBePerformed = false;
            GetComponent<Image>().color = nonActive;
        }

    }

    public void perform()
    {

        if (item != null && quality != null)
        {
            
            GameManager.instance.activateSkill(item, quality);
        }
        else
        {
            throw new Exception("Set the item and quality first!"); 
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
		Debug.Log (item.id);
        if (item != null && !canBePerformed){
            conditionWindow.setText(item.name,quality);
        } else if (item.id.Equals("bPlan")){ //если бизнес-план, то вызываем мини-игру
			CreateBPlan bPlanPanel = GameManager.instance.businessPlanPanel;
			bPlanPanel.gameObject.SetActive (true);
			bPlanPanel.bPlan = this;
			if (Tutorial.instance.isActive())
			{
				Tutorial.instance.onSelectQuality();
			}
		} else {
            perform();
        }
        
    }
}
