﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

public class ConditionsUI : MonoBehaviour {

    public Text text;
    public Animator animator;
    ItemDatabase db;
    void Start()
    {
        db = ItemDatabase.instance;    
    }
    bool running = false;
    public void setText(string input)
    {
        if (!running)
        {
            animator.SetBool("enable", true);
            text.text = input;
            StartCoroutine(countdown());
        }
    }
    public void setText(Item item)
    {
        if (!running)
        {
            animator.SetBool("enable", true);
            text.text = "Условия для " + item.name + " не соблюдены: \n";
            List<string> conditions = new List<string>();
            Debug.Log(item.possibleQualities.Count);
            foreach (string str in item.possibleQualities[item.possibleQualities.Count-1].conditions)
            {
                conditions.Add(db.getItem(str).name);
            }
            foreach (string str in conditions)
            {
                text.text += str + " \n";
            }
            //start coroutine and remove this shit
            StartCoroutine(countdown());
        }
    }
    public void setText(string itemName, Quality quality)
    {
        if (!running)
        {
            animator.SetBool("enable", true);
            text.text = "Условия для " + itemName + " не соблюдены: \n";
            List<string> conditions = new List<string>();
            foreach (string str in quality.conditions)
            {
                conditions.Add(db.getItem(str).name);
            }
            foreach (string str in conditions)
            {
                text.text += str + " \n";
            }
            //start coroutine and remove this shit
            StartCoroutine(countdown());
        }
    }
    public IEnumerator countdown()
    {
        running = true;
        yield return new WaitForSeconds(2);
        animator.SetBool("enable",false);
        running = false;
    }
}
