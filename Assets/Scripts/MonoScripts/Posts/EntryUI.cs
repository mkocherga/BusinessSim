﻿using UnityEngine.UI;
using UnityEngine;

public class EntryUI : MonoBehaviour
{

    public GameObject up, down;
    public Image img;
    public Text value;
    GameManager manager;
    void Awake()
    {
        manager = GameManager.instance;
    }
    public void fillContent(string paramName, int value)
    {
        if (manager.commonImages.ContainsKey(paramName))
        {
            img.sprite = manager.commonImages[paramName];
        }
        else
        {
            Sprite image = Resources.Load<Sprite>("Icons/Common/" + paramName);
            manager.commonImages.Add(paramName, image);
            img.sprite = image;
        }
        if (paramName == "capital")
        {
            if (value > 0)
            {
                down.SetActive(true);

            }
            else if (value < 0)
            {
                up.SetActive(true);
            }
        }
        else
        {
            if (value < 0)
            {
                down.SetActive(true);

            }
            else if (value > 0)
            {
                up.SetActive(true);
            }
        }
        this.value.text = value.ToString();
    }
}
