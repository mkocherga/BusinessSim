﻿using UnityEngine;
using UnityEngine.UI;

public class TempMainPost : MonoBehaviour {

    public Image mainImage, smallIcon;
    public Image[] stars;
    public Text header, mainText, dateStamp;

    public void fill(string imagePath, string itemid, string header, string message)
    {
        GameManager manager = GameManager.instance;
        ItemDatabase db = ItemDatabase.instance;
        string path = "Images/" + imagePath;
        mainImage.sprite = Resources.Load<Sprite>(path);
        if (db.sprites.ContainsKey(itemid))
        {
            smallIcon.sprite = db.sprites[itemid];
        }
        Item item = ItemDatabase.instance.getItem(itemid);
        int level = item.workingQuality.qualityId;
        for (int i = 1; i < level + 1; i++)
        {
            stars[i - 1].enabled = true;
        }
        this.header.text = header;
        dateStamp.text = ((int)manager.currentTime / manager.multiplayer).ToString() + " День";
        mainText.text = message;
    }
}
