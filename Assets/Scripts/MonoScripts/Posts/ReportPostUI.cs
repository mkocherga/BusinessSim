﻿using UnityEngine;
using UnityEngine.UI;

public class ReportPostUI : MonoBehaviour
{


    public Text mainheader;
    public GameObject postBody, postEntry;
    GameManager manager;
    void Awake()
    {
        manager = GameManager.instance;
    }
    public void setPost(ReportPost post)
    {
        mainheader.text = post.mainHeader;
        int num = 0;
        foreach (ReportPost.ParamValue param in post.paramValues)
        {
            GameObject newParam = Instantiate(postEntry);
            newParam.transform.SetParent(postBody.transform, false);
            if (num % 2 == 1)
            {
                newParam.GetComponent<Image>().enabled = true;
            }
            if (manager.commonImages.ContainsKey(param.id))
            {
                newParam.transform.GetChild(0).GetComponent<Image>().sprite = manager.commonImages[param.id];
            }
            else
            {
                Sprite image = Resources.Load<Sprite>("Icons/Common/" + param.id);
                manager.commonImages.Add(param.id, image);
                newParam.transform.GetChild(0).GetComponent<Image>().sprite = image;
            }

            newParam.transform.GetChild(1).GetComponent<Text>().text = param.header;
            GameObject periodValue = newParam.transform.GetChild(2).gameObject;
            try
            {
                if (int.Parse(param.periodValue) < 0)
                {
                    periodValue.transform.GetChild(1).gameObject.SetActive(true);
                }
                else if (int.Parse(param.periodValue)>0)
                {
                    periodValue.transform.GetChild(0).gameObject.SetActive(true);
                }
            }
            catch
            {
               Debug.Log("unknown error");
            }

            periodValue.GetComponent<Text>().text = param.periodValue;
            newParam.transform.GetChild(3).GetComponent<Text>().text = param.totalValue;
        }
    }
}
