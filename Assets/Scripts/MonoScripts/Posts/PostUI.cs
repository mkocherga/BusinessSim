﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PostUI : MonoBehaviour
{
    public GameObject accept;
    public GameObject decline;
    public Text mainText, header, dataStamp;
    //public Image mainImage;
    Slot availableSlot;
    ExtraPost post;
    //because we have 2 identical posts in extra window and in main window, we use link to the copy to remove it after some action;
    GameObject secondPost;
    GameManager manager = GameManager.instance;
    PostsManager postManager = PostsManager.instance;
    bool isDanger = false;

    public void fill(ExtraPost post, GameObject secondPost)
    {
        this.post = post;
        this.secondPost = secondPost;
        //string path = "Images/" + post.item.id;
        //mainImage.sprite = Resources.Load<Sprite>(path);
        header.text = post.header;
        mainText.text = post.message;
    }
    
    public void setTextToDanger()
    {
        accept.transform.GetChild(0).GetComponent<Text>().text = "Чинить";
        isDanger = true;
    }
 #region interaction_with_post
    bool interactable = true;
    void Start()
    {
        EventTrigger trigger = accept.GetComponent<EventTrigger>();
        EventTrigger.Entry entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => { onAccept(); });
        trigger.triggers.Add(entry);
        
        trigger = decline.GetComponent<EventTrigger>();
        entry = new EventTrigger.Entry();
        entry.eventID = EventTriggerType.PointerClick;
        entry.callback.AddListener((eventData) => { onDecline(); });
        trigger.triggers.Add(entry); 
    }
    void OnEnable()
    {
        Slot.slotStateChanged += canBePerformed;
        canBePerformed();
    }
    void OnDisable()
    {
        Slot.slotStateChanged -= canBePerformed;
    }
    void onDecline()
    {
        if (interactable)
        {
            Destroy(secondPost);
            if (isDanger)
            {
				post.item.Repair();
            }
			post.item.deactivate ();
            postManager.removeExtraPost(post);
            manager.removeCurrentlyWorking(post.item.id);
            manager.redrawStats();
            Destroy(gameObject);
        }
    }
    public void canBePerformed()
    {
        availableSlot = manager.getAvailableSlot();
        if (post == null)
        {
            return;
        }
        if (availableSlot != null && post.item.isEnoughMoney())
        {
            accept.GetComponent<Button>().interactable = true;
            interactable = true;
        }
        else
        {
            accept.GetComponent<Button>().interactable = false;
            interactable = false;
        }
    }
    void onAccept()
    {
        if (interactable)
        {
            Destroy(secondPost);
            postManager.removeExtraPost(post);
            if (isDanger)
            {
                availableSlot.setItem(System.DateTime.Now, post.item, 1 * manager.multiplayer);
            }
            else
            {
                availableSlot.setItem(System.DateTime.Now, post.item, post.item.workingQuality.time * manager.multiplayer);
            }

            Destroy(gameObject);
        }
    }
#endregion
}
