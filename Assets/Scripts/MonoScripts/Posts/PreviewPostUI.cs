﻿using UnityEngine;
using UnityEngine.UI;

public class PreviewPostUI : MonoBehaviour {
    public Text headerText,dateStamp;
    public Image icon;
    public Image mainImage;

    //may be some data
    public void fill(string imagePath, string itemid, string header)
    {
        GameManager manager = GameManager.instance;
        string path = "Images/" + imagePath;
        mainImage.sprite = Resources.Load<Sprite>(path);
        if (ItemDatabase.instance.sprites.ContainsKey(itemid))
        {
            icon.sprite = ItemDatabase.instance.sprites[itemid];
        }
        dateStamp.text = ( manager.currentTime / manager.multiplayer).ToString();
    }
}
