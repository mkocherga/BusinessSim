﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class DateDelimeterPost : IPost
{
    public String date;
    public DateDelimeterPost(string date)
    {
        this.date = date;
    }
    public override void createPost()
    {
        PostsManager postManager = PostsManager.instance;
        GameObject datePost = postManager.createPrefab(postManager.datePrefab);
        datePost.transform.GetChild(0).GetComponent<Text>().text ="День " + date;
        datePost.transform.SetParent(postManager.mainContent, false);
        datePost.transform.SetSiblingIndex(1);
    }
}
