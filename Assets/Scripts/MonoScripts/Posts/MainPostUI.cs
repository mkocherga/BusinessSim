﻿using UnityEngine;
using UnityEngine.UI;

public class MainPostUI : MonoBehaviour {

    public Image mainImage, smallIcon;
    public Image[] stars;
    public Text header, mainText, dateStamp;
    public GameObject starsBg, sideImg;
    public GameObject closePref,headerMain,grid,sideLine;
    public Text closedHeader;
    MainPost post;
    public void fill(MainPost post)
    {
        this.post = post;
        GameManager manager = GameManager.instance;
        ItemDatabase db = ItemDatabase.instance;
//        string path = "Images/" + post.itemid;
        string itemid = post.itemid;
        if (post.itemid.Contains("_"))
        {
             itemid = itemid.Split('_')[1];
        }
        

        if (db.sprites.ContainsKey(itemid))
        {
            smallIcon.sprite = db.sprites[itemid];
        }
        try{
            int level = db.getItem(itemid).workingQuality.qualityId;
        for (int i = 1; i < level + 1; i++)
        {
            stars[i - 1].enabled = true;
        }
        }catch{
            Debug.Log("hmm");
        }
        this.header.text = post.header.ToUpper();
        closedHeader.text = post.header.ToUpper();

        if(post.time == -1)
        {
            post.time = manager.daysLeft + 1;
        }
        mainText.text = post.message;
        
        dateStamp.text = post.time.ToString();
    }
    bool closed = true;
    public void expand()
    {
        {
            if (closed)
            {
            //    Debug.Log("opening");
                headerMain.SetActive(false);
                mainText.gameObject.SetActive(false);
                grid.SetActive(false);
                sideLine.SetActive(false);
                closed = false;
                closePref.SetActive(true);
            }
            else
            {
                headerMain.SetActive(true);
                mainText.gameObject.SetActive(true);
                grid.SetActive(true);
                sideLine.SetActive(true);
                closePref.SetActive(false);
                closed = true;
             //   Debug.Log("closing");
            }
        }
    }
}
