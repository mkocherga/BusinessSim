﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Newtonsoft.Json;
using System.Collections;
using UnityEngine.EventSystems;
public class GameManager : MonoBehaviour
{

    #region publicvariables

    public static GameManager instance { get; set; }
    public int currentTime { get; set; }

    
    public AudioController audioController;
    public ConditionsUI conditionsWindow;
    public AudioSource dangerSound;
    public Sprite normal, selected;
    
    public Button businessButton;

    public Text dailyProfitText;
    public Text netCostText;
    public Text goalText;
    public Text checkText;
    public Text weekText;
    public Text monthText;
    public Text DayText;
    public Text businessPowerText;
    public Text customersText;
    public Text headerText;
    public Text moneyText;

    public GameObject businessPanel, marketingPanel;
    public GameObject loadingImg;
    public GameObject marketingSkills;
    public GameObject businessSkills;
    public GameObject buttonPrefab;
    public Chooser chooser;
    public GameObject skills;
	public CreateBPlan businessPlanPanel;

    public ItemSlotUI[] chooserSlots;
    public Slider finantialGoalSlider;
    public Slot[] slots;

    public List<String> currentlyWorking = new List<String>();
    public List<float> statsToRevert = new List<float>();

    public int multiplayer;

    #endregion

    #region privatevariables
    PlayerStats stats;
    PostsManager postsManager;
    
    ItemDatabase db;
    //one ot three main buttons
    List<BeforeDanger> beforeDangers = new List<BeforeDanger>();
//	List<SuperDanger> superDangers = new List<SuperDanger>();
    public String bType = "";
	public bool isProVersion = false;
    List<String> loadedSkills;
    
    public PlayerStats getStats()
    {
        return stats;
    }

	public void setBusinessType(String type, bool isPro)
    {
        bType = type;
		isProVersion = isPro;
    }
    #endregion

    #region initialization

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }

        postsManager = PostsManager.instance;
        db = ItemDatabase.instance;
        db.createTestData();
        if (PlayerPrefs.GetInt("first") == 1)
        {
            Debug.Log("loading stats");
            loadStats();
        }
        else
        {
            //start new game
            timeAppClosed = DateTime.Now;
			stats = PlayerStats.getInstance();
           
            startGame();
            PlayerPrefs.SetInt("first", 1);
			beforeDangers = db.databaseOfBeforeDangers;
//			superDangers = db.databaseOfSuperDangers;
           // loadBeforeDangers();
        }
        StartCoroutine("tictac");
        headerText.text = "Моя кофейня";
        headerText.fontSize = 55;
    }
    public Dictionary<string, Sprite> commonImages = new Dictionary<string, Sprite>();
    public GameObject extraImg;
    public void checkForExtra()
    {
        extraImg.SetActive(PostsManager.instance.hasExtraContent());
    }
    public void loadStats()
    {
        GameSave load = JsonConvert.DeserializeObject<GameSave>(PlayerPrefs.GetString("save"), JsonSettings.settings);

        stats = load.stats;
        currentTime = load.currentTime;
        currentMaxTime = load.currentMaxTimes;
        timeAppClosed = load.timeAppClosed;
        db.loadData(load.databaseOfItems);
        loadedSkills = load.activeSkills;
        beforeDangers = load.beforeDangers;
        double past = getTimePast();
        if (past > 0)
        {
            currentTime += getTimePast();
        }
        postsManager.setPosts(load.posts);

        currentlyWorking = load.currentlyWorking;

        startGame();
        //populate slots
        for (int i = 0; i < slots.Length; i++)
        {
            slots[i].loadParams(load.slots[i]);
        }
        postsManager.loadPosts();
        checkForExtra();
        refresh();
        redrawStats();
        clearPendingNotifications();
    }
    public void clearPendingNotifications()
    {
        if (currentlyWorking != null)
        {
            foreach (String str in currentlyWorking)
            {
                LocalNotification.CancelNotification(str.GetHashCode());
            }
        }
    }
    public void startGame()
    {
        moneyText.text = stats.capital.ToString();
        businessPowerText.text = stats.businessPower.ToString();
        checkText.text = stats.baseCheck.ToString();
        loadSkills();
    }
//    void loadBeforeDangers()
//    {
//        foreach (Danger danger in db.databaseOfDangers)
//        {
//            if (danger.id.Contains("before"))
//            {
//                beforeDangers.Add(danger);
//            }
//        }
//    }
//	void loadSuperDangers()
//	{
//		foreach (SuperDanger danger in db.databaseOfSuperDangers)
//		{
//			superDangers.Add(danger);
//		}
//		if (isProVersion) 
//		{
//			foreach (SuperDanger danger in db.databaseOfSuperDangers)
//			{
//				superDangers.Add(danger);
//			}
//		}
//	}

    public void playAudio()
    {
        dangerSound.Play();
    }

    void loadSkills()
    {
        skillButtons = new Dictionary<Item, SkillCounter>();
        foreach (Item item in db.databaseOfItems)
        {

            GameObject button = Instantiate(buttonPrefab);
            SkillCounter counter = button.GetComponent<SkillCounter>();
            if (item.type == "marketing")
            {
                //create button in marketing field
                button.transform.SetParent(marketingSkills.transform, false);
            }
            else
            {
                //create button in business field
                button.transform.SetParent(businessSkills.transform, false);
            }
            if (!item.available(activeSkills) || (currentlyWorking != null && currentlyWorking.Contains(item.id)))
            {
                counter.setInteractable(false);
            }

            EventTrigger trigger = button.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerUp;
            entry.callback.AddListener((eventData) => { OnClick(item); });
            trigger.triggers.Add(entry);
            if (db.sprites.ContainsKey(item.id))
            {
                button.transform.GetChild(1).GetComponent<Image>().sprite = db.sprites[item.id];
            }
            button.transform.GetChild(0).GetComponent<Text>().text = item.name;
            skillButtons.Add(item,counter);
            if (loadedSkills != null && loadedSkills.Contains(item.id))
            {
                activeSkills.Add(item.id, item);
                Debug.Log(item.id);
                foreach(String str in loadedSkills)
                {
                    Debug.Log(str);
                }
                skillButtons[item].GetComponent<SkillCounter>().showLevel(item);
            }

        }

    }
    #endregion

    #region timeSystem

    DateTime timeAppClosed;
    DateTime currentMaxTime;
    public Slider daySlider;
    public int daysLeft;
    public IEnumerator tictac()
    {
        while (true)
        {
            if (checkIfProcessing())
            {
                if (currentTime > 0 && currentTime % (multiplayer) == 0)
                {
                    performEveryDayAction();
                    daysLeft = (currentTime / multiplayer);
                    postsManager.addPost(new DateDelimeterPost(daysLeft.ToString()));
                }
                currentTime++;
                daySlider.value = (float)((currentTime) % multiplayer) / (float)multiplayer;

            }
            checkForExtra();
            yield return new WaitForSeconds(1);

        }

    }
    public void setMultiplayer(Text text){
        try{
            multiplayer = Int32.Parse(text.text);
        }catch{
            multiplayer = 30;
        }
    }
    public void performEveryDayAction()
    {
        //stats.capital += stats.capital_per_day;

		//Увеличивающий коэфицент если игрок использовал инструмент "work"
        float buff = 1;
        if(workBuff){
            buff =1.05f;
            workBuff=false;
        }

		int maxPossibleCustomers = (int) (stats.customers * buff); //Максимальное возможное кол-во клиентов
		int possibleCustomers = UnityEngine.Random.Range (maxPossibleCustomers / 2, maxPossibleCustomers + 1); //Сколько посетителей захотело сегодня к нам прийти
		int dailyCustomers = (int)(Mathf.Min(possibleCustomers, stats.businessPower*buff)); //Сколько посетителей мы смогли принять
		float checkProfit = stats.check + stats.netCost; // Выручка с одного чека
		stats.dailyProfit = dailyCustomers * checkProfit; // Подсчитываем общую выручку за день

		stats.capital += stats.dailyProfit + stats.capital_per_day; //К капиталу прибавляем дневную выручку и дневые расходы
        //everyday|week|month pst

        
        showAppropriatePost();
        audioController.playSound(1);
        if (currentTime > 5*multiplayer )
        {
            manageDangers();
        }

        redrawStats();
    }
    public void removeActiveSkill(String skillid)
    {
        activeSkills.Remove(skillid);
        Item temp = db.getItem(skillid);
        skillButtons[temp].hide();
        skillButtons[temp].showLevel(temp);
        refresh();
    }
#region dangers
    //сделать нормально
    public void addCurrentlyWorking(String str)
    {
        if (!currentlyWorking.Contains(str))
        {
            currentlyWorking.Add(str);
        }
    }
    void manageDangers()
    {
        //first remove all before dangers, that are not currently active(e.g i learned skill)
        beforeDangers.RemoveAll(danger => activeSkills.ContainsKey(danger.id.Split(' ')[1]));

        foreach (BeforeDanger danger in beforeDangers)
        {
            try
            {
                string[] temp = danger.id.Split(' ');
                Item toCheck = db.getItem(temp[1]);
                int random = UnityEngine.Random.Range(0, 100);
                if (random < toCheck.chanceDangerBefore)
                {
                    postsManager.addPost(new DangerMainPost(danger.header, danger.message, "before_" + temp[1],false));
                    //before только штрафы
                    stats.capital -= danger.capital;
                    dangerSound.Play();
                }
            }
            catch
            {
                Debug.Log("Item name is incorrect");
            }
        }

//		foreach (Danger danger in siDangers)
//		{
//			try
//			{
//				string[] temp = danger.id.Split(' ');
//				Item toCheck = db.getItem(temp[1]);
//				int random = UnityEngine.Random.Range(0, 1000);
//				if (random < toCheck.chanceDangerBefore*10)
//				{
//					postsManager.addPost(new DangerMainPost(danger.header, danger.message, "before_" + temp[1],false));
//					//before только штрафы
//					stats.capital -= danger.capital;
//					dangerSound.Play();
//				}
//			}
//			catch
//			{
//				Debug.Log("Item name is incorrect");
//			}
//		}

        List<string> breaksThatHappened = new List<string>();
        List<string> lostsThatHappened = new List<string>();

        foreach (Item item in activeSkills.Values)
        {
			//Если еще нет текущей поломки
            if (!currentlyWorking.Contains(item.id))
            {
				//Сначала пытаемся сломать (break)
                int random = UnityEngine.Random.Range(0, 100);
                if (random < item.actualQuality.chanceDangerBreak)
                {
                    BreakDanger danger = db.databaseOfBreakDangers.Find(x => x.id == "break " + item.id);
                    if (danger != null)
                    {
                        breaksThatHappened.Add(item.id);
                        postsManager.addPost(new DangerPost(danger.header, danger.message, "danger", item));
                        addCurrentlyWorking(item.id);
                        item.Break(danger);
                        Debug.Log(item.activeDanger);
                    }
                }
				//если не сломали, то пытаемся сломать кончательно (lost)
                else
                {
                    random = UnityEngine.Random.Range(0, 100);
                    LostDanger danger = db.databaseOfLostDangers.Find(x => x.id == "lost " + item.id);
                    if (danger != null && random < item.actualQuality.chanceDangerLost)
                    {
                        postsManager.addPost(new DangerMainPost(danger.header, danger.message, "lost_" + item.id, false));
						item.deactivate ();
                    }
                }
            }
        }
        foreach (String str in lostsThatHappened)
        {
            removeActiveSkill(str);
        }
        foreach (String str in breaksThatHappened)
        {
            removeActiveSkill(str);
            skillButtons[db.getItem(str)].setInteractable(false);
        }
        if (lostsThatHappened.Count > 0 || breaksThatHappened.Count > 0)
        {
            audioController.playSound(2);
        }
        refresh();
    }
#endregion dangers
    public void showAppropriatePost()
    {
        if (currentTime == 0)
        {
            //to skip initial post
            return;
        }
        ReportPost post = null;
//        if (currentTime % (30 * 7 * (multiplayer)) == 0)
//        {
//            //prinnt month
//            List<ReportPost.ParamValue> values = new List<ReportPost.ParamValue>();
//            values.Add(new ReportPost.ParamValue("Капитал", stats.capital.ToString(), (stats.capital - stats.monthlyCapital).ToString(), "capital"));
//            stats.monthlyCapital = stats.capital;
//            values.Add(new ReportPost.ParamValue("Мощность Производства", ((int)stats.businessPower).ToString(), ((int)(stats.businessPower - stats.monthlyBPower)).ToString(), "bPower"));
//            stats.monthlyBPower = stats.businessPower;
//            values.Add(new ReportPost.ParamValue("Охват аудитории", ((int)stats.customers).ToString(), ((int)(stats.customers - stats.monthlyCustomers)).ToString(), "customers"));
//            stats.monthlyCustomers = stats.customers;
//            values.Add(new ReportPost.ParamValue("Дневной доход", stats.dailyProfit.ToString(), ((int)(stats.dailyProfit - stats.monthlyProfit)).ToString(), "dailyProfit"));
//            stats.monthlyProfit = stats.dailyProfit;
//            values.Add(new ReportPost.ParamValue("Средний чек", stats.check.ToString(), (stats.check - stats.monthlyCheck).ToString(), "check"));
//            stats.monthlyCheck = stats.check;
//            values.Add(new ReportPost.ParamValue("Себестоимость", stats.netCost.ToString(), (stats.netCost - stats.monthlyNetCost).ToString(), "netCost"));
//            stats.monthlyNetCost = stats.netCost;
//            post = new ReportPost("Eжемесячный отчет:", values);
//
//        }
//        else if (currentTime % (7 * (multiplayer)) == 0)
//        {
//            //print week
//            List<ReportPost.ParamValue> values = new List<ReportPost.ParamValue>();
//            values.Add(new ReportPost.ParamValue("Капитал", stats.capital.ToString(), (stats.capital - stats.weeklyCapital).ToString(), "capital"));
//            stats.weeklyCapital = stats.capital;
//            values.Add(new ReportPost.ParamValue("Мощность Производства", ((int)stats.businessPower).ToString(), ((int)(stats.businessPower - stats.weeklyBPower)).ToString(), "bPower"));
//            stats.weeklyBPower = stats.businessPower;
//            values.Add(new ReportPost.ParamValue("Охват аудитории", ((int)stats.customers).ToString(), ((int)(stats.customers - stats.weeklyCustomers)).ToString(), "customers"));
//            stats.weeklyCustomers = stats.customers;
//            values.Add(new ReportPost.ParamValue("Дневной доход", stats.dailyProfit.ToString(), ((int)(stats.dailyProfit - stats.weeklyProfit)).ToString(), "dailyProfit"));
//            stats.weeklyProfit = stats.dailyProfit;
//            values.Add(new ReportPost.ParamValue("Средний чек", stats.check.ToString(), (stats.check - stats.weeklyCheck).ToString(), "check"));
//            stats.weeklyCheck = stats.check;
//            values.Add(new ReportPost.ParamValue("Себестоимость", stats.netCost.ToString(), (stats.netCost - stats.weeklyNetCost).ToString(), "netCost"));
//            stats.weeklyNetCost = stats.netCost;
//            post = new ReportPost("Еженедельный отчет:", values);
//        }
        if (currentTime % (multiplayer) == 0)
        {
            //print day
            List<ReportPost.ParamValue> values = new List<ReportPost.ParamValue>();
            values.Add(new ReportPost.ParamValue("Капитал", stats.capital.ToString(), (stats.capital - stats.dailyCapital).ToString(), "capital"));
            stats.dailyCapital = stats.capital;
            values.Add(new ReportPost.ParamValue("Мощность Производства", ((int)stats.businessPower).ToString(), ((int)(stats.businessPower - stats.dailyBPower)).ToString(), "bPower"));
            stats.dailyBPower = stats.businessPower;
            values.Add(new ReportPost.ParamValue("Охват аудитории", ((int)stats.customers).ToString(), ((int)(stats.customers - stats.dailyCustomers)).ToString(), "customers"));
            stats.dailyCustomers = stats.customers;
            values.Add(new ReportPost.ParamValue("Дневной доход", stats.dailyProfit.ToString(), ((int)(stats.dailyProfit - stats.lastDailyProfit)).ToString(), "dailyProfit"));
            stats.lastDailyProfit = stats.dailyProfit;
            values.Add(new ReportPost.ParamValue("Средний чек", stats.check.ToString(), (stats.check - stats.dailyCheck).ToString(), "check"));
            stats.dailyCheck = stats.check;
            values.Add(new ReportPost.ParamValue("Себестоимость", stats.netCost.ToString(), (stats.netCost - stats.dailyNetCost).ToString(), "netCost"));
            stats.dailyNetCost = stats.netCost;
            post = new ReportPost("Ежедневный отчет:", values);
        }
        else
        {
            return;
        }
        postsManager.addPost(post);
    }
    public bool checkIfProcessing()
    {

        foreach (Slot slot in slots)
        {
            if (!slot.isEmpty())
            {
                return true;
            }
        }
        return false;
    }

    public void notifyAboutTime(DateTime time)
    {
        if (currentMaxTime.CompareTo(time) < 0)
        {
            currentMaxTime = time;
        }
    }

    public string InsertSpaces(string number)
    {
        number = number.Replace(" ","");
        if(number.Length>4){
            for (int i = 0; i < number.Length; i += 4)
            {
                number = number.Insert(number.Length - i, " ");
            }
        }
        return number;

    }
    IEnumerator AddingCor(Text textField,int targetMoney,bool rouble)
    {
        
        string input = textField.text.Replace("&","");
        
        int money = Int32.Parse(input.Replace(" ", ""));
        int addingCount = targetMoney-money;
        
        if (targetMoney > money)
        {
            while (money < targetMoney && addingCount >30)
            {
                money += (int)Mathf.Floor(addingCount / 30f);
                textField.text = InsertSpaces(money.ToString());
                if (rouble)
                    textField.text += "&";
                yield return new WaitForSeconds(Time.deltaTime);
            }
            if (money > targetMoney || money< targetMoney)
            {
                money = targetMoney;
                textField.text = InsertSpaces(money.ToString());
                if (rouble)
                    textField.text += "&";
            }
        }
        else
        {
            while (targetMoney < money )
            {
                money += (int)Mathf.Floor(addingCount / 30f);
                textField.text = InsertSpaces(money.ToString());
                if (rouble)
                    textField.text += "&";
                yield return new WaitForSeconds(Time.deltaTime);
            }
            if (targetMoney > money )
            {
                money = targetMoney;
                textField.text = InsertSpaces(money.ToString());
                if (rouble)
                    textField.text += "&";
            }
            
        }
       
    }


    public int getTimePast()
    {
        double answer = 0;
        if (DateTime.Now.CompareTo(currentMaxTime) > 0)
        {

            answer = (currentMaxTime - timeAppClosed).TotalSeconds;
            if (answer > 0)
            {
                return (int)answer;
            }
            else
            {
                return 0;
            }

        }
        else
        {
            answer = (DateTime.Now - timeAppClosed).TotalSeconds;
            if (answer > 0)
            {
                return (int)answer;
            }
            else
            {
                return 0;
            }
        }

    }

    #endregion

    #region workingWithSkills

    public Dictionary<String, Item> activeSkills = new Dictionary<String, Item>();
    bool workBuff = false;
    public void addLearnedSkill(Item item)
    {
        if(item.id=="work"){
            //вроде как можно просто это все удалить, проверь когда будет время
            item.activate();
            item.actualQuality = null;
            workBuff = true;
        }else{
            audioController.playSound(0);
            if(item.activeDanger!=null){
				item.Repair ();
                Debug.Log(item.workingQuality.qualityLevel);
                item.activate();
                activeSkills.Add(item.id, item);
            }else{
                if (activeSkills.ContainsKey(item.id))
                {
                    item.deactivate();
                }
                else
                {
                    activeSkills.Add(item.id, item);
                }
                item.activate();
            }
            skillButtons[db.getItem(item.id)].showLevel(item);
        }
        currentlyWorking.Remove(item.id);
        
        redrawStats();
        refresh();
    }
    public void removeCurrentlyWorking(string id)
    {
        currentlyWorking.Remove(id);
        refresh();
    }

    
    public void setNormalImage(Image btn)
    {
        btn.sprite = normal;
    }
    public void setSelectedImage(Image btn)
    {
        btn.sprite = selected;
    }
    //не нужен но может понадобится
    public void work()
    {
        int timeNeeded = multiplayer - currentTime % multiplayer;
        currentTime += timeNeeded;
        //may be play some animation to get attention and add some delay?
        performEveryDayAction();
        daySlider.value = 0;
        skills.SetActive(false);
    }


    //new ones
    //creates button for each new item in database
    public Dictionary<Item, SkillCounter> skillButtons;
    


    //check if any new condition is satisfied, if so, move next
    public void refresh()
    {
        foreach (Item item in skillButtons.Keys)
        {
            if (item.available(activeSkills) )
            {
                if (!currentlyWorking.Contains(item.id))
                {
                    skillButtons[item].setInteractable(true);
                }
                else
                {
                    skillButtons[item].showTimer();
                }
            }
        }
    }

    //listeners for buttons when we choose quality of object
    void OnClick(Item item)
    {
        if (!skillButtons[item].canClick)
        {
            skillButtons[item].canClick = true;
            return;
        }
        if (skillButtons[item].isInteractable())
        {
            chooser.gameObject.SetActive(true);
			if (Tutorial.instance.isActive())
            {
                Tutorial.instance.onChooser();
            }
            Sprite image = null;
            if (db.sprites[item.id] != null)
            {
                image = db.sprites[item.id];
            }
            chooser.fill(item.description, image, item.name);
            
            
            List<int> occupiedSlots = new List<int>();

            int currentQualityLevel = -1;
            if (activeSkills.ContainsKey(item.id) && item.actualQuality != null)
            {
                currentQualityLevel = item.actualQuality.qualityId;
            }

            foreach(Quality quality in item.possibleQualities)
            {
                if (quality.qualityId > currentQualityLevel)
                {
                    ItemSlotUI slot = chooserSlots[quality.qualityId];
                    slot.gameObject.SetActive(true);
                    slot.setValues(item, quality);
                    occupiedSlots.Add(quality.qualityId);
                }
            }

            for(int i = 0; i < 4; i++)
            {
                if (!occupiedSlots.Contains(i))
                {
                    chooserSlots[i].gameObject.SetActive(false);
                }
            }
        }
        else
        {
            //show hint, why we can't perform this action
            if (!currentlyWorking.Contains(item.id))
            {
                conditionsWindow.setText(item);
            }
            else
            {
                conditionsWindow.setText("Выбранный предмет уже выполнется или ожидает вашего решения");
            }
        }
    }



    public Slot getAvailableSlot()
    {
        foreach (Slot slot in slots)
        {
            if (slot.isEmpty())
            {
                return slot;
            }
        }
        
        return null;
    }
    public Slot currentSlot { get; set; }
    //when we selected skill to learn
    public void activateSkill(Item item, Quality quality)
    {
        item.workingQuality = quality;

        currentSlot.setItem(DateTime.Now, item, item.workingQuality.time * multiplayer);
        skills.SetActive(false);
        chooser.gameObject.SetActive(false);
        businessPanel.SetActive(false);
        marketingPanel.SetActive(false);

        //what is this for?)
        if (skillButtons.ContainsKey(item))
        {
            skillButtons[item].setInteractable(false);
            skillButtons[item].showTimer();
        }
    }
    #endregion

    #region WorkingWithStats
    //replace with activate/deactivate
    public void changeCapital(int money)
    {
        stats.capital += money;
        redrawStats();
    }
    
    public void calculateDays()
    {
        int month = (int)(currentTime / multiplayer / 30);
        int week = (int)((currentTime - (month * 30) * multiplayer) / 7 / multiplayer);
        int day = (int)(currentTime - (month * 30 + week * 7) * multiplayer) / multiplayer;
        DayText.text = "День " + (day + 1);
        weekText.text = "Неделя " + (week + 1);
        monthText.text = "Месяц " + (month + 1);
    }
    public void redrawStats()
    {
        StartCoroutine(AddingCor(moneyText,(int)stats.capital,true));
        businessPowerText.text = InsertSpaces(((int)stats.businessPower).ToString());
        customersText.text = InsertSpaces(((int)stats.customers).ToString());
        StartCoroutine(AddingCor(checkText,(int)stats.check,false));
        StartCoroutine(AddingCor(netCostText,(int)stats.netCost,false));
        StartCoroutine(AddingCor(dailyProfitText,(int)stats.dailyProfit,true));
        goalText.text = InsertSpaces(stats.capital.ToString()) + " / " + InsertSpaces(((int)stats.goal).ToString());
        finantialGoalSlider.value = stats.capital / stats.goal;
        calculateDays();
    }
    #endregion

    #region saveSystem

    public void sendNotif()
    {
        foreach (Slot slot in slots)
        {
            Item item = slot.getItem();
            if (item != null)
            {
                TimeSpan time = slot.getDateFinish().Subtract(DateTime.Now);
                if (time.TotalSeconds > 0)
                {
                    if (slot.result.Equals("MT"))
                    {
                        LocalNotification.SendNotification((int)item.id.GetHashCode(), time.Minutes * 60 * 1000 + time.Seconds * 1000 + time.Milliseconds, "Внимание ", item.id + "требует вашего внимания!", new Color32(0xff, 0x44, 0x44, 255));
                    }
                    else
                    {
                        LocalNotification.SendNotification((int)item.id.GetHashCode(), time.Minutes * 60 * 1000 + time.Seconds * 1000 + time.Milliseconds, "Внимание", "Работа над " + item.id + " закончена, слот свободен!", new Color32(0xff, 0x44, 0x44, 255));
                    }
                }
            }
        }
    }

    bool wasPaused;

    void OnApplicationPause(bool pauseStatus)
    {
        if (pauseStatus)
        {
            sendNotif();
            timeAppClosed = DateTime.Now;
            wasPaused = true;
        }
        saveData();
    }

    void OnApplicationFocus(bool hasFocus)
    {
        if (hasFocus && wasPaused)
        {
            currentTime += getTimePast();
            wasPaused = false;
            clearPendingNotifications();
        }
    }
    void OnApplicationQuit()
    {
        saveData();
        sendNotif();
        Debug.Log("Application ending after " + Time.time + " seconds");
    }

    public void saveData()
    {

        List<Slot.JsonSlot> slotsToSave = new List<Slot.JsonSlot>();
        foreach (Slot slot in slots)
        {
            slotsToSave.Add(slot.getJsonSlot());
        }

        List<String> activatedSkills = new List<String>();
        foreach (String str in activeSkills.Keys)
        {
            activatedSkills.Add(str);
        }
        string output;
        GameSave save = new GameSave(slotsToSave, stats, currentTime, currentMaxTime, DateTime.Now, activatedSkills, db.databaseOfItems, postsManager.getPosts(), currentlyWorking, beforeDangers);
        output = JsonConvert.SerializeObject(save, JsonSettings.settings);
        PlayerPrefs.SetString("save", output);
    }
    public void loadData()
    {
        PlayerPrefs.GetString("save");
    }

    public class GameSave
    {
        public List<Slot.JsonSlot> slots;
        public PlayerStats stats;
        public int currentTime;
        public DateTime currentMaxTimes, timeAppClosed;
        public List<String> activeSkills;
        public List<IPost> posts;
        public List<Item> databaseOfItems;
        public List<BeforeDanger> beforeDangers;
        public List<String> currentlyWorking;

        [JsonConstructor]
        public GameSave(List<Slot.JsonSlot> slots, PlayerStats stats, int currentTime,
        DateTime currentMaxTimes, DateTime timeAppClosed, List<string> activeSkills, List<Item> databaseOfItems,
        List<IPost> posts, List<String> currentlyWorking, List<BeforeDanger> beforeDangers)
        {
            this.currentlyWorking = currentlyWorking;
            this.slots = slots;
            this.stats = stats;
            if (currentTime > 0)
            {
                this.currentTime = currentTime;
            }
            else
            {
                this.currentTime = 0;
            }
            this.currentMaxTimes = currentMaxTimes;
            this.timeAppClosed = timeAppClosed;
            this.activeSkills = activeSkills;
            this.databaseOfItems = databaseOfItems;
            this.posts = posts;
            this.beforeDangers = beforeDangers;
        }
    }
    #endregion

	public void CreateBPlan(){
		businessPlanPanel.createBPlan ();
	}
		

}

