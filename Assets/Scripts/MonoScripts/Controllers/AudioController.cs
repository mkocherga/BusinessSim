﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

	public AudioClip[] clips;
	public AudioSource source;
	public void playSound(int id){
		source.clip = clips[id];
		source.Play();
	}



}
