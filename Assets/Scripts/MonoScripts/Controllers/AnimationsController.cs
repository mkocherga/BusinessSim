﻿using UnityEngine;

public class AnimationsController : MonoBehaviour {
    

    public Animator paramsAnimator;
    public void expandParams() {
        Tutorial tutor = Tutorial.instance;
		if (!tutor.isActive() || (tutor.isActive() && tutor.canBeClosed))
        {
            bool expand = paramsAnimator.GetBool("expand");
            paramsAnimator.SetBool("expand", !expand);
			if (tutor.isActive())
            {
                if (!expand)
                {
                    tutor.onExpand();
                }
                else
                {
                    tutor.onClose();
                }
            }
        }
    }

}
