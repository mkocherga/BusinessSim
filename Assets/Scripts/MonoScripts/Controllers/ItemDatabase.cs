﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
public class ItemDatabase : MonoBehaviour {
    string bType = "";
	bool isProVersion = false;

    public static ItemDatabase instance { get; set; }
	public List<Item> databaseOfItems { get; set; }
	public List<BeforeDanger> databaseOfBeforeDangers { get; set; }
    public List<BreakDanger> databaseOfBreakDangers { get; set; }
	public List<LostDanger> databaseOfLostDangers { get; set; }
	public List<SuperDanger> databaseOfSuperDangers { get; set; }
    public Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();

	public void setBusinessType(string typeName, bool isPro)
    {
        bType = typeName;
		isProVersion = isPro;
    }

	void Awake(){
		if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
        }
	}
    public void loadImages()
    {
        for(int i = 0; i < databaseOfItems.Count; i++)
        {
            string pathToIcons = "Icons/" + "Items" + "/";
            string path = pathToIcons + databaseOfItems[i].id;
            Sprite sprite = Resources.Load<Sprite>(path);
            sprites.Add(databaseOfItems[i].id, sprite);
        //    Debug.Log(path);
        }
    }

    public void createTestData() {
		string pathToBusinessParams = bType;
		if (isProVersion) {
			pathToBusinessParams += "Pro";
		}
		databaseOfItems = JsonConvert.DeserializeObject<List<Item>>(Resources.Load<TextAsset>("JSON/"+pathToBusinessParams).ToString());
		databaseOfBeforeDangers = JsonConvert.DeserializeObject<List<BeforeDanger>>(Resources.Load<TextAsset>("JSON/BeforeDanger").ToString());
        databaseOfBreakDangers = JsonConvert.DeserializeObject<List<BreakDanger>>(Resources.Load<TextAsset>("JSON/BreakDanger").ToString());
		databaseOfLostDangers = JsonConvert.DeserializeObject<List<LostDanger>>(Resources.Load<TextAsset>("JSON/LostDanger").ToString());
		databaseOfSuperDangers = JsonConvert.DeserializeObject<List<SuperDanger>>(Resources.Load<TextAsset>("JSON/SuperDanger").ToString());
		if (isProVersion) 
		{
			List<SuperDanger> temp = JsonConvert.DeserializeObject<List<SuperDanger>>(Resources.Load<TextAsset>("JSON/SuperDangerPro").ToString());
			databaseOfSuperDangers.AddRange (temp);
		}
        loadImages();
	}
	public void loadData(List<Item> items){
		databaseOfItems = items;
	}
	public Item getItem(string id){
		foreach (Item item in databaseOfItems){
			if(item.id.Equals(id)){
				return item;
			}
		}
		return null;
	}
}
