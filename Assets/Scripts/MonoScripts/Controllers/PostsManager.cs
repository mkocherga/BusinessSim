﻿using System.Collections.Generic;
using UnityEngine;

public class PostsManager : MonoBehaviour
{

    public Transform mainContent, extraContent;
    public GameObject mainPostPrefab, extraPostPrefab, reportPostPrefab, entryPrefab,datePrefab,closedPostPrefab,previewPostPrefab;
    List<IPost> posts = new List<IPost>();
    public ExtraAttentionUI extra;
    public static PostsManager instance;
    PlayerStats stats;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
		stats = PlayerStats.getInstance();
    }

    public void setPosts(List<IPost> posts)
    {
        this.posts = posts;
    }
    public bool hasExtraContent()
    {
        return extraContent.childCount > 0 ? true : false;
    }
    public void addPost(IPost post)
    {
        posts.Add(post);
        post.createPost();
        if (post is ReportPost || post is DateDelimeterPost)
        {
            Debug.Log("report post here");
            return;
        }
        extra.gameObject.SetActive(true);

        extra.addPost(post);
       

    }
    //private void Start()
    //{
    //    extra.gameObject.SetActive(false);
    //}
    public void removeExtraPost(IPost post)
    {
        posts.Remove(post);
    }

    public List<IPost> getPosts()
    {
        return posts;
    }

    public GameObject createPrefab(GameObject prefab)
    {
        return Instantiate(prefab);
    }

    public void createEntries(GameObject parent, Item item, bool success)
    {

        Quality working = item.workingQuality;
        Quality previoudQuality = item.actualQuality;

        if (success && previoudQuality != working)
        {

            if (working.businesPower > 0)
            {
                int prevVal = 0;
                if (previoudQuality != null )
                {
                    prevVal = (int)((previoudQuality.businesPower) * stats.basePower);
                }
                GameObject entry = Instantiate(entryPrefab);
                entry.GetComponent<EntryUI>().fillContent("bPower", (int)(stats.basePower * (working.businesPower) - prevVal));
                entry.transform.SetParent(parent.transform, false);
            }
            if (working.check > 0)
            {
                int prevVal = 0;
                if (previoudQuality != null )
                {
                    prevVal = (int)((previoudQuality.check) * stats.baseCheck);
                }
                GameObject entry = Instantiate(entryPrefab);
                entry.GetComponent<EntryUI>().fillContent("check", (int)(stats.baseCheck * (working.check) - prevVal));
                entry.transform.SetParent(parent.transform, false);
            }
            if (working.customers > 0)
            {
                int prevVal = 0;
                int curVal = 0;
                int total = (int)(stats.baseCustomers * working.customers);
                if (previoudQuality != null)
                {
                    prevVal = (int)stats.customers;
                    curVal = (int)(stats.customers + stats.baseCustomers * working.customers - previoudQuality.customers * stats.baseCustomers);
                    total = curVal - prevVal;
                    //prevVal = (int)((previoudQuality.customers ) * stats.baseCustomers);
                }
                GameObject entry = Instantiate(entryPrefab);
                entry.GetComponent<EntryUI>().fillContent("customers",total);
                entry.transform.SetParent(parent.transform, false);
            }
            if (working.netCost > 0)
            {
                int prevVal = 0;
                if (previoudQuality != null)
                {
                    prevVal = (int)((previoudQuality.netCost)* stats.baseNetCost);
                }
                GameObject entry = Instantiate(entryPrefab);
                entry.GetComponent<EntryUI>().fillContent("netCost",(int)(stats.baseNetCost * (working.netCost) - prevVal));
                entry.transform.SetParent(parent.transform, false);

            }
            if (working.capital > 0)
            {
                GameObject entry = Instantiate(entryPrefab);
                entry.GetComponent<EntryUI>().fillContent("capital", (int)working.capital);
                entry.transform.SetParent(parent.transform, false);
            }
        }
    }

    public void loadPosts()
    {
        foreach (IPost post in posts)
        {
            post.createPost();
        }
    }
}
