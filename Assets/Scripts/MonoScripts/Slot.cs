﻿using System.Collections;
using UnityEngine;
using System;
using UnityEngine.UI;
using Newtonsoft.Json;
using UnityEngine.EventSystems;


public class Slot : MonoBehaviour,IPointerClickHandler
{
    public delegate void onSlotsStateChange();
    public static event onSlotsStateChange slotStateChanged;

    public GameObject removeImg;
    public Sprite normal, occupied;
    public Image imageTimer,itemIcon,bg;
    public Text completeionText;
    Item currentItem;
    DateTime whenItemLocked;
    float timeOccupied;
    public GameManager GameManager;
    PostsManager PostsManager;
    public double remainingTime;
    public String result;
    public DateTime getDateFinish()
    {
        DateTime whenToFinish = whenItemLocked.AddSeconds(timeOccupied);
        return whenToFinish;
    }
    
    public void setItem(DateTime when, Item item, float timeOccupied)
    {       
        currentItem = item;
        whenItemLocked = when;
        GameManager.addCurrentlyWorking(currentItem.id);
        this.timeOccupied = timeOccupied;
        prepareSlot();
        if (slotStateChanged != null)
        {
            slotStateChanged();
        }
        determineResult();
    }
    void prepareSlot()
    {
        completeionText.gameObject.SetActive(true);
        bg.enabled = true;
        itemIcon.enabled = true;
        GetComponent<Image>().sprite = occupied;
        completeionText.text = timeOccupied.ToString();
        imageTimer.fillAmount = 1;
        itemIcon.sprite = ItemDatabase.instance.sprites[currentItem.id];
    }
    public Item getItem()
    {
        return currentItem;
    }
    public void setNewTime(DateTime when, float timeOccupied)
    {
        whenItemLocked = when;
        this.timeOccupied = timeOccupied;
        determineResult();
    }
    public void reset()
    {
        currentItem = null;
        GetComponent<Image>().sprite = normal;
        imageTimer.fillAmount = 0;
        bg.enabled = false;
        itemIcon.enabled = false;
        if (slotStateChanged != null)
        {
            slotStateChanged();
        }
        removeImg.SetActive(false);
    }
    public void removeItemFromSlot()
    {
        GameManager.removeCurrentlyWorking(currentItem.id);
        reset();
    }
    public IEnumerator run()
    {
        while (true)
        {
            if (currentItem != null && currentItem.id != "")
            {
                // remainingTime = timeOccupied - (DateTime.Now-whenItemLocked).TotalSeconds;
                GameManager.notifyAboutTime(whenItemLocked.AddSeconds(timeOccupied));
                double deltaTime = (timeOccupied - (DateTime.Now - whenItemLocked).TotalSeconds);
                if (deltaTime > 0)
                {
                    imageTimer.fillAmount = (float)(deltaTime / timeOccupied);
                    completeionText.text = ((int)deltaTime +1).ToString();
                }
                else
                {
                    Debug.Log(result);
                    //chances of success/fail/moretime 
                    if (result.Equals("success"))
                    {
                        PostsManager.addPost(new MainPost(currentItem.successHeader, currentItem.workingQuality.messageSuccess, currentItem.id,true));
                        if (currentItem.activeDanger != null)
                        {
                            GameManager.changeCapital((int)(currentItem.workingQuality.capital * 0.1));
                        }
                        else
                        {
                            GameManager.changeCapital((int)(currentItem.workingQuality.capital));
                        }
                        GameManager.addLearnedSkill(currentItem);
                        Debug.Log("added");
                        
                    }

                    else if (result.Equals("fail"))
                    {
                        PostsManager.addPost(new MainPost("Не удалось выполнить", currentItem.workingQuality.messageFailure, currentItem.id,false));
                        if (currentItem.activeDanger == null)
                        {
                            GameManager.changeCapital((int)currentItem.workingQuality.ifFailCapital);
                        }
                        GameManager.removeCurrentlyWorking(currentItem.id);
                        
                    }
                    else if (result.Equals("MT"))
                    {
                        Debug.Log("created");
                        PostsManager.addPost(new ExtraPost("Необходимо больше времени", currentItem.workingQuality.messageMoreTime, gameObject.name, currentItem));
                    }
                    reset();
                    completeionText.gameObject.SetActive(false);
                }
                
            }
            yield return null;
        }
    }
    public void determineResult()
    {
        int curChance = UnityEngine.Random.Range(0, 100);

        if (curChance < currentItem.workingQuality.chanceOfSuccess)
        {
            result = "success";
        }

        else if (curChance > currentItem.workingQuality.chanceOfSuccess
        && curChance < currentItem.workingQuality.chanceOfSuccess + currentItem.workingQuality.chanceOfFailure)
        {
            result = "fail";
        }
        else if (curChance > currentItem.workingQuality.chanceOfSuccess + currentItem.workingQuality.chanceOfFailure
        && curChance < currentItem.workingQuality.chanceOfSuccess + currentItem.workingQuality.chanceOfFailure + currentItem.workingQuality.chanceOfMoreTime)
        {
            result = "MT";
        }
        if (result == "")
        {
            result = "success";
        }
    }

    public bool isEmpty()
    {
        return currentItem == null ? true : false;
    }
    
    void Start()
    {
        PostsManager = PostsManager.instance;
        
        StartCoroutine("run");
        if(currentItem!=null){
            GetComponent<Image>().sprite = ItemDatabase.instance.sprites[currentItem.id];
        }
    }
    public JsonSlot getJsonSlot()
    {
        return new JsonSlot(currentItem, whenItemLocked, timeOccupied, result);
    }

    public void loadParams(JsonSlot slotData)
    {
        Item item = null;
        if (slotData.currentItem != null)
        {
            item = ItemDatabase.instance.getItem(slotData.currentItem.id);
        }
        if (item != null)
        {
            whenItemLocked = slotData.whenItemLocked;
            currentItem = item;
            timeOccupied = slotData.timeOccupied;
            result = slotData.result;
            prepareSlot();
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (isEmpty())
        {
            GameManager.currentSlot = this;
            GameManager.businessButton.onClick.Invoke();
            GameManager.skills.SetActive(true);
			if (Tutorial.instance.isActive())
            {
                Tutorial.instance.onSkills();
            }
        }
        else
        {
            removeImg.SetActive(!removeImg.activeSelf);
        }
    }

    //I know that there is better way, but haven't known it at the time it was written , at least it works
    public class JsonSlot
    {
        public Item currentItem;
        public DateTime whenItemLocked;
        public float timeOccupied;
        public string result;

        [JsonConstructor]
        public JsonSlot(Item currentItem, DateTime whenItemLocked, float timeOccupied, string result)
        {
            this.currentItem = currentItem;
            this.whenItemLocked = whenItemLocked;
            this.timeOccupied = timeOccupied;
            this.result = result;
        }
    }
}
