﻿using UnityEngine;
using System;

public class CreateBPlan: MonoBehaviour
{
	public ItemSlotUI bPlan;
	public void createBPlan(){
		if (bPlan != null) {
			if (checkBPlan()) {
				if (Tutorial.instance.isActive()) {
					Tutorial.instance.onCreateBPlan ();
				} else {
					bPlan.perform ();
					this.gameObject.SetActive (false);
				}
			} else {
				//TODO: вывести сообщение с просьбой заполнить все поля и тд
			}
		} else {
			Debug.Log("Something wrong with bPlan");
		}
	}

	bool checkBPlan(){
		return true;
	}
}


