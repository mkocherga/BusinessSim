﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StartGame : MonoBehaviour {

	public Button contBtn;
    ButtonAnimation anim;
	public GameObject manager;
    public GameObject exceptionMessage;
    public GameObject loadingPanel;
    public GameObject mainUI;
    public GameObject appBar;
    public GameObject gameStartUI;
    public GameObject startingParamsUI;
    public GameObject businessChooserUI;
    public GameObject tutorial;
    public Image paramsBussinesIcon;
    public Text paramsBussinesText;
    public Sprite[] Bimages;

    int startingCapital = 100000;
    int goal = 0;
    public Text capitalText;
    PlayerStats stats;
    public float time;
    string bType;
	bool isProVersion = false;
	//Метод, который вызывается при нажатии кнопки выбора бизнеса
	public void openParams(string bType)
    {
		string[] temp = bType.Split (' ');
		this.bType = temp[0];
		Debug.Log (this.bType);
		if (temp.Length>1 && temp[1].Equals("Pro")){
			this.isProVersion = true;
			Debug.Log ("Pro");
		}
        businessChooserUI.SetActive(false);
        startingParamsUI.SetActive(true);
		switch (this.bType) {
		case "Coffe":
			paramsBussinesIcon.sprite = Bimages [0];
			paramsBussinesText.text = "Кофейня";
			break;
		case "Salon":
			Debug.Log ("салон");
			paramsBussinesIcon.sprite = Bimages [1];
			paramsBussinesText.text = "Салон красоты";
			break;
		case "Furniture":
			paramsBussinesIcon.sprite = Bimages [2];
			paramsBussinesText.text = "Мебельная фабрика";
			break;
		}
		if (isProVersion) {
			paramsBussinesText.text += " Pro";
		}
    }
    public void onNewGameButtonPressed(ButtonAnimation anim)
    {
        this.anim = anim;
        StartCoroutine(playStartBtnAnim());
    }

    IEnumerator playStartBtnAnim()
    {
        anim.startAnimation(time);
        yield return new WaitForSeconds(time*3);
        appBar.SetActive(true);
        businessChooserUI.SetActive(true);
        gameStartUI.SetActive(false);
    }
    
	void Start () {
        if (PlayerPrefs.GetInt("restart") == 1)
        {
            gameStartUI.SetActive(false);
            appBar.SetActive(true);
            businessChooserUI.SetActive(true);
            PlayerPrefs.SetInt("restart", 0);
        }

		if(PlayerPrefs.GetInt("first")==1){
			contBtn.interactable = true;
		}
	}
//	public void setBusinessType(Text type){
//		businesType = type.text;	
//	}
	public void setStartingCapital(string capital){
		startingCapital  = int.Parse(capital);
		capitalText.text = capital + " руб";
	}
	public void setStartingCapital(Text capital){
		startingCapital = int.Parse(capital.text);
		capitalText.text = capital.text + " руб";
	}
	public void setGoal(Text goal){
		this.goal = int.Parse(goal.text);
        if (exceptionMessage.activeSelf)
        {
            exceptionMessage.SetActive(false);
        }
	}
    public void loadOldGame()
    {
        manager.SetActive(true);
    }
	public void loadGame(){
        if (goal > startingCapital&&goal < int.MaxValue)
        {
            loadingPanel.SetActive(true);
            tutorial.SetActive(true);
            StartCoroutine(startnewGame());
        }
        else
        {   
            exceptionMessage.SetActive(true);
            Text text = exceptionMessage.GetComponent<Text>();
            if(goal ==0){
                text.text = "Вы должны установить финансовую цель!";
            }else{
                if(goal<startingCapital){
                    text.text = "Слишком маленькая цель";
                }else
                {
                    text.text = "Слишком оптимистичная цель.Установите более низкую финансовую цель";
                }
            }
            
        }
    }
    public IEnumerator startnewGame()
    {
        yield return new WaitForSeconds(0.1f);
        mainUI.SetActive(true);
        startingParamsUI.SetActive(false);
        //create game manager and setup with this parameters
        PlayerPrefs.DeleteAll();
		ItemDatabase.instance.setBusinessType(bType, isProVersion);
        manager.SetActive(true);
		GameManager.instance.setBusinessType(bType, isProVersion);
//		PlayerStats stats = reset(bType);
		GameManager.instance.getStats().resetStats(bType);
		PlayerStats stats = GameManager.instance.getStats();
	//	Debug.Log (stats);
        stats.capital = startingCapital;
        stats.dailyCapital = startingCapital;
        stats.goal = goal;
        // GameManager.instance.goal.text = goal;
        GameManager.instance.redrawStats();
        loadingPanel.SetActive(false);
    }
}
