﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class StartMenuButtonsController : MonoBehaviour {

	void onStartClick(Button btn,Text text)
    {
        
    }
    public Image btn;
    public Text text;

    public void playAnim()
    {
        StartCoroutine(playBtnAnim());
    }
    IEnumerator playBtnAnim()
    {
        btn.color = Color.white;
        yield return new WaitForSeconds(0.1f);
        btn.color = Color.black;
        yield return new WaitForSeconds(0.1f);
        btn.color = Color.white;
    }

}
