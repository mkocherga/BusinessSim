﻿using UnityEngine.UI;
using UnityEngine;

public class ChangeTextColor : MonoBehaviour
{

    public Color enabledCol, disabledCol;

    public void setEnabledColor(Text text)
    {
        text.color = enabledCol;
    }
    public void setDisabledColor(Text text)
    {
        text.color = disabledCol;
    }
}
