﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAnimation : MonoBehaviour
{
    public Sprite[] images;
    public Color[] colors;
    public Text text;
    public Image myImage;
    bool isRunning = false;
    public void startAnimation(float time)
    {
        this.time = time;
        if (!isRunning)
        {
            isRunning = true;
//            Debug.Log("animatingg");
            StartCoroutine(animate());
        }
        
    }
    float time;
    public IEnumerator animate()
    {
        
        myImage.sprite = images[1];
        text.color = colors[1];
        yield return new WaitForSeconds(time);
        myImage.sprite = images[0];
        text.color = colors[0];
        yield return new WaitForSeconds(time);
        myImage.sprite = images[1];
        text.color = colors[1];
        yield return new WaitForSeconds(time);
        myImage.sprite = images[0];
        text.color = colors[0];
        isRunning = false;
    }

}
