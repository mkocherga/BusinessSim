﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
[JsonObject(MemberSerialization.OptIn)]
public class TestPost : MonoBehaviour  {

	GameObject someObject;
	[JsonProperty]
	public string header,body;
	// Use this for initialization
	
	public TestPost(string header, string body){
		this.header = header;
		this.body = body;
	}

}
