﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Newtonsoft.Json;
[JsonObject(MemberSerialization.OptIn)]
public class TestPostManager : MonoBehaviour {
	[JsonProperty]
	List<TestPost> posts ;
	
	[JsonConstructor]
	public TestPostManager(List<TestPost> post){
		this.posts = post;
	}
	string output;
	public void save(){
		posts = new List<TestPost>();
		TestPost first = new TestPost("header1","body1");
		posts.Add(first);
		posts.Add(new TestPost("header2","post2"));
		output = JsonConvert.SerializeObject(this);
		posts.Remove(first);
		Debug.Log(output);
		print();
		
	}
	public void print(){
		foreach(TestPost post in posts){
			Debug.Log(post.header + " " + post.header);
		}
	}
	public void load(){
		JsonConvert.PopulateObject(output,this);
		Debug.Log("After");
		print();
	}
}
