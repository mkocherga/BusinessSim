﻿using UnityEngine;

public class ExtraPanelToggle : MonoBehaviour {
	
	public GameObject extrapanel;

	public void toggle(){
		extrapanel.SetActive(!extrapanel.activeSelf);
	}
	void OnDisable(){
		extrapanel.SetActive(false);
	}
	
}
